<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class EventsTable extends Table{
    
        public function getMyEvent(){
        //Retourne dans un tableau tous les fighters d'un joueur
        return ($this->find()->order(['id' => 'DESC'])->toArray());
        //return ($this->find()->where(['player_id' => $session->read('player_id')])->order(['level' => 'DESC'])->toArray());
    }
        public function getAllEvents(){
            $lastday= new Time ('1 day ago');
            $DBquery= $this->find('all')->where(["date >"=>$lastday])->order(['Events.date'=>'DESC'])->toArray();
            return($DBquery);   
        }
             
        public function newEventLevelUp($fighters)
        {
                $event = $this->newEntity();
                $event->name = $fighters->name. ' est désormais niveau '. $fighters->level;
                $event->date=date("Y-m-d H:i:s");
                $event->date =time();
                $event->coordinate_x=$fighters->coordinate_x;
                $event->coordinate_y=$fighters->coordinate_y;
                $this->save($event);

        }
    public function newEventKill($fighter,$target_fighter)
    {
         $event = $this->newEntity();
         $event->name = $fighter->name. ' tue ' . $target_fighter->name.' !';
         $event->date=date("Y-m-d H:i:s");
                $event->date =time();
                $event->coordinate_x=$fighter->coordinate_x;
                $event->coordinate_y=$fighter->coordinate_y;
                $this->save($event);

    }
    public function newEventAttack($fighter,$target_fighter)
    {
                $event=$this->newEntity();
                $event->name = $fighter->name. ' attaque ' . $target_fighter->name.' et gagne 1 xp !'.$target_fighter->name.' perd '.$fighter->skill_strength.' point de vie ! ';
                $event->date=date("Y-m-d H:i:s");
                $event->date =time();
                $event->coordinate_x=$fighter->coordinate_x;
                $event->coordinate_y=$fighter->coordinate_y;
                $this->save($event);
    }
    public function newEventAttackFail($fighter,$target_fighter)
    {
                $event=$this->newEntity();
                $event->name = $fighter->name. ' attaque ' . $target_fighter->name.' et rate !';
                $event->date=date("Y-m-d H:i:s");
                $event->date =time();
                $event->coordinate_x=$fighter->coordinate_x;
                $event->coordinate_y=$fighter->coordinate_y;
                $this->save($event);
    }   
    public function newEventStuff($fighter,$stuff)
    {
                $event=$this->newEntity();
                $event->name = $fighter->name. ' à recuperer un equipement de type '.$stuff->type. ' et gagne +'.$stuff->bonus;
                $event->date=date("Y-m-d H:i:s");
                $event->date =time();
                $event->coordinate_x=$fighter->coordinate_x;
                $event->coordinate_y=$fighter->coordinate_y;
                $this->save($event);
    }
    
    public function eventActivity(){
        //renvoie le nb d'event sur les 7 derniers jours
        $last1= new Time ('1 day ago');
        $last2= new Time ('2 day ago');
        $last3= new Time ('3 day ago');
        $last4= new Time ('4 day ago');
        $last5= new Time ('5 day ago');
        $last6= new Time ('6 day ago');
        $last7= new Time ('7 day ago');
        $day = new Time ('1 day');
        $stacknb = array();
        $stackday = array();
        $eventweek = $this->find('all')->where(["date >"=>$last7])->order(['Events.date'=>'DESC'])->first();
        $day0 = $eventweek->date;
        $day1 = $day0->modify('-1 days');
        $day2 = $day0->modify('-2 days');
        $day3 = $day0->modify('-3 days');
        $day4 = $day0->modify('-4 days');
        $day5 = $day0->modify('-5 days');
        $day6 = $day0->modify('-6 days');
        $nbj0 = $this->find('all')->where(["date >"=>$last1])->count();
        $nbj1 = ($this->find('all')->where(["date >"=>$last2])->count()) - ($this->find('all')->where(["date >"=>$last1])->count());
        $nbj2 = ($this->find('all')->where(["date >"=>$last3])->count()) - ($this->find('all')->where(["date >"=>$last2])->count());
        $nbj3 = ($this->find('all')->where(["date >"=>$last4])->count()) - ($this->find('all')->where(["date >"=>$last3])->count());
        $nbj4 = ($this->find('all')->where(["date >"=>$last5])->count()) - ($this->find('all')->where(["date >"=>$last4])->count());
        $nbj5 = ($this->find('all')->where(["date >"=>$last6])->count()) - ($this->find('all')->where(["date >"=>$last5])->count());
        $nbj6 = ($this->find('all')->where(["date >"=>$last7])->count()) - ($this->find('all')->where(["date >"=>$last6])->count());
        array_push($stacknb, $nbj0, $nbj1, $nbj2, $nbj3, $nbj4, $nbj5, $nbj6);
        array_push($stackday, $day0, $day1, $day2, $day3, $day4, $day5, $day6);
        return array($stacknb, $stackday);
    }
    
}