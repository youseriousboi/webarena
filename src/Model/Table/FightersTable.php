<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class FightersTable extends Table{
       public function optionCconst()
    {
        $ptAction=3;
        $recoverySecond=10;
        return array($ptAction,$recoverySecond);
    }
    public function setReferenceDate($fighter)
    {
                list($ptAction,$recoverySecond)=$this->optionCconst();
                $fighter->next_action_time=$fighter->next_action_time->addSeconds($recoverySecond);
                $this->save($fighter);
    }
      public function setReferenceDateBase($fighter)
    {
                 list($ptAction,$recoverySecond)=$this->optionCconst();
                $fighter->next_action_time =Time::now();
                  $fighter->next_action_time=$fighter->next_action_time->subSeconds($ptAction*$recoverySecond);
                $this->save($fighter);
    }
    public function setActionPoints($fighter)
    {
        list($ptAction,$recoverySecond)=$this->optionCconst();
        $currentTime=Time::now();
        $refDate=$fighter->next_action_time; 
        $diff=$refDate->diff($currentTime);
        $tmpconst=$ptAction*$recoverySecond;
        if(($diff->s>$tmpconst)||($diff->i>0)) // if the refence date is higher than the time for getting the maximum action point, set it
        {
            $this->setReferenceDateBase($fighter);
        }
        else
        {
            if($diff->s<=$recoverySecond)
                {
                  return('NoAction');
            }
        
        }
        if($diff->s>$recoverySecond) // if i have an action point
        {
           $this->setReferenceDate($fighter);
            return('Action');
        } 
      
       
    }
    public function ligcol(){
        $col=15;
        $lig=10;
        return array($col,$lig);
    }
       public function test(){
        
        return("ok");
    }
    
    public function getBestFighter(){       
        return ($this->find('all')->order(['xp'=>'DESC'])->first());
    }
    
    public function getBestFightersBonus(){    
        $toolsTable = TableRegistry::get('tools');
        $stackbonus = array();
        $bestf = $this->find('all')->order(['xp'=>'DESC'])->limit(5)->toArray();
        foreach($bestf as $fighter){
            array_push($stackbonus, $toolsTable->getStuffByID($fighter->id));
        }
        return array ($bestf, $stackbonus);
    }
    
    public function getBestFighters(){    
        return ($this->find('all')->order(['xp'=>'DESC'])->toArray());
    }
    
    public function countReroll($playerID){
        return($this->find()->where(['player_id' => $playerID])->count());
    }
    
    public function getMyFighterByPlayerID($playerID){
        //Retourne dans un tableau tous les fighters d'un joueur
        return ($this->find()->where(['player_id' => $playerID])->order(['level' => 'DESC'])->toArray());
    }
    
    public function getMyFighterById($id){
        //Retourne dans un tableau tous les fighters d'un joueur
        return ($this->find()->where(['player_id' => $id])->first());
        //return ($this->find()->where(['player_id' => $session->read('player_id')])->order(['level' => 'DESC'])->toArray());
    }
    
     public function getMyFighterByFighterId($id){
        return ($this->find()->where(['id' => $id])->first());        
    }
    
          public function getAll(){
        //Retourne dans un tableau tous les fighters d'un joueur
        return ($this->find()->all());
    }
     public function deleteFighter($fighter)
    {
        return($this->delete($fighter));
    }
     public function getMyFighterName($fightername) {
        //Retourne dans un tableau tous les fighters d'un joueur
        return ($this->find()->where(['name' => $fightername])->first());
    }
    
    public function getRandomPosx() {
        list($col, $lig) = $this->ligcol();
        return($posx = rand(1, $col));
    }

    public function getRandomPosy() {
            list($col,$lig)=$this->ligcol();
        return($posx = rand(1, $lig));
    }
    
    public function createNewFighter($playerID, $pseudo){
        $fightersTable = TableRegistry::get('fighters');
        $fighter = $fightersTable->newEntity();
        $fighter->name = $pseudo; 
        $fighter->player_id = $playerID;     
        //check the position sin't used by another fighter
        $isMissPlaced = true;
        $posX;
        $posY;
        $isPlacedOnABusyCell = false;
        $allFighters = $this->getAll();
        while($isMissPlaced){
            $posX = $this->getRandomPosx();
            $posY = $this->getRandomPosy();    
            
            foreach ($allFighters as $fighterFromTable){
                if($fighterFromTable->coordinate_x == $posX && $fighterFromTable->coordinate_y == $posY){
                    $isPlacedOnABusyCell = true;
                }
            }
            if( false == $isPlacedOnABusyCell ){
                $isMissPlaced = false; //sortie de la boucle
            }
            $isPlacedOnABusyCell = false;
        }
        $fighter->coordinate_x = $posX;
        $fighter->coordinate_y = $posY;
        $fighter->level = 0;
        $fighter->xp = 0;
        $fighter->skill_sight = 2;
        $fighter->skill_strength = 1;
        $fighter->skill_health = 5;
        $fighter->current_health = $fighter->skill_health;
        $fighter->next_action_time = Time::now();
        $fighter->guild_id = null;
        if($fightersTable->save($fighter)){
            return $fighter;
        }
    }

   public function setPos($fighter){
       //$this=Table::get('fighters');
       //$fighter = $FightersTable->get($fightername);
      // $fighter->name = "BOB" 
       $fighter->coordinate_x = $this->getRandomPosx();
       $fighter->coordinate_y = $this->getRandomPosy();
       $this->save($fighter);
   }
   public function deplacement($fighter,$dir){
       $toolsTable = TableRegistry::get('Tools');
       list($col,$lig)=$this->ligcol();
        $actionPossible=$this->setActionPoints($fighter);
       if($actionPossible==='Action'){
       if(($dir=='droite') && ($fighter->coordinate_x<$col) && count($this->getTargetByXY($fighter->coordinate_x + 1, $fighter->coordinate_y)) != 1 && count($toolsTable->getStuffByPos($fighter->coordinate_x + 1, $fighter->coordinate_y)) != 1){
        $fighter->coordinate_x += 1;
        $this->save($fighter);
       }
       else if(($dir=='gauche') && ($fighter->coordinate_x>1) && count($this->getTargetByXY($fighter->coordinate_x - 1, $fighter->coordinate_y)) != 1 && count($toolsTable->getStuffByPos($fighter->coordinate_x - 1, $fighter->coordinate_y)) != 1){
        $fighter->coordinate_x -= 1;
        $this->save($fighter);
       }
       else if(($dir=='haut') && ($fighter->coordinate_y>1) && count($this->getTargetByXY($fighter->coordinate_x, $fighter->coordinate_y - 1)) != 1 && count($toolsTable->getStuffByPos($fighter->coordinate_x, $fighter->coordinate_y - 1)) != 1){
        $fighter->coordinate_y -= 1;
        $this->save($fighter);
       }
       else if(($dir=='bas') && ($fighter->coordinate_y<$lig) && count($this->getTargetByXY($fighter->coordinate_x, $fighter->coordinate_y + 1)) != 1 && count($toolsTable->getStuffByPos($fighter->coordinate_x, $fighter->coordinate_y + 1)) != 1){
        $fighter->coordinate_y += 1;
        $this->save($fighter);
       }
   }
   }
   public function getTargetByXY($coordx, $coordy){
       return ($this->find()->where(['coordinate_x' => $coordx, 'coordinate_y' => $coordy])->first());
   }
   
   public function targetAttacked($fighter, $target_fighter) {
       //calcul action d'attaque reussite ou pas
        $rand = rand(1, 20);
        $seuil = 10 + $target_fighter->level - $fighter->level;
        $eventTable=TableRegistry::get('events');
        
        //$eventTable->toArray();
      //  pr($eventTable);
        //Attaque reussie
        if ($rand > $seuil) {
            $target_fighter->current_health -= $fighter->skill_strength;
            //S'il tue la cible
            if ($target_fighter->current_health <= 0) {
                $fighter->xp += $target_fighter->level + 1;
               $eventTable->newEventKill($fighter,$target_fighter);
              // $eventTable->newEventLvlAvailable($fighter);
                $this->deleteFighter($target_fighter);
                //return('kill');
            } else {
                //S'il ne tue pas gagne 1 point d'xp
                $fighter->xp += 1;
              $eventTable->newEventAttack($fighter,$target_fighter);
                //return('hit');
            }
        } else { //attaque ratée
            //return('fail');
            //echo '<script type="text/javascript">window.alert("Attack failed sorry BOI!!");</script>';
            $eventTable->newEventAttackFail($fighter,$target_fighter);
        }
        $this->save($fighter);
        $this->save($target_fighter);
    }

    public function attack($fighter,$dir){
       list($col,$lig)=$this->ligcol();
        $actionPossible=$this->setActionPoints($fighter);
       if($actionPossible==='Action'){
       if(($dir=='right')&&($fighter->coordinate_x<$col)){
           $target_x = $fighter->coordinate_x + 1;
           $target_y = $fighter->coordinate_y;
           $target_fighter = $this->getTargetByXY($target_x, $target_y);
           //S'il y a qqun a la position attaquée
           if(count($target_fighter) != 0){
           //if($target_fighter->coordinate_x > 0 &&  $target_fighter->coordinate_x < 16 && $target_fighter->coordinate_y > 0 && $target_fighter->coordinate_y < 16 && $target_fighter->current_health > 0){
               $this->targetAttacked($fighter, $target_fighter);
           }
           else{
               //return('noone');
           }
       }
        else if (($dir == 'left') && ($fighter->coordinate_x > 1)) {
            $target_x = $fighter->coordinate_x - 1;
            $target_y = $fighter->coordinate_y;
            $target_fighter = $this->getTargetByXY($target_x, $target_y);
            //S'il y a qqun a la position attaquée
            if (count($target_fighter) != 0) {
                $this->targetAttacked($fighter, $target_fighter);
            } else {
                //return('noone');
            }
        } else if(($dir=='up')&&($fighter->coordinate_y>1)){
            $target_x = $fighter->coordinate_x;
            $target_y = $fighter->coordinate_y - 1;
            $target_fighter = $this->getTargetByXY($target_x, $target_y);
            //S'il y a qqun a la position attaquée
            if (count($target_fighter) != 0) {
                $this->targetAttacked($fighter, $target_fighter);
            } else {
                //return('noone');
            }
       
       }
       else if(($dir=='down')&&($fighter->coordinate_y<$lig)){
            $target_x = $fighter->coordinate_x;
            $target_y = $fighter->coordinate_y + 1;
            $target_fighter = $this->getTargetByXY($target_x, $target_y);
            //S'il y a qqun a la position attaquée
            if (count($target_fighter) != 0) {
                $this->targetAttacked($fighter, $target_fighter);
            } else {
                //return('noone');
            }
       }
       }
   }
   public function level_up($stat, $id){
       $fightersTable = TableRegistry::get('fighters');
        $eventTable = TableRegistry::get('events');
       //$fighters = $this->getMyFighterById($id);
       $fighters = $fightersTable->get($id);
       if($stat == 'health'){
            $fighters->skill_health += 3;
            $fighters->current_health = $fighters->skill_health;
           //newEventHealth
       } else if($stat == 'attack'){
            $fighters->skill_strength += 1;
       } else if($stat == 'sight'){
            $fighters->skill_sight += 1;
       }
       $fighters->level ++;
       $fightersTable->save($fighters);
       $eventTable->newEventLevelUp($fighters);
      
               }
   
   public function upstuff($fighter, $stuff){
       $ToolsTable = TableRegistry::get('tools');
       $eventTable= TableRegistry::get('events');
       //$eventTable = TableRegistry::get('events');
       if($stuff->type == 'V'){
           $fighter->skill_sight += $stuff->bonus;
       }else if($stuff->type == 'D'){
           $fighter->skill_strength += $stuff->bonus;
       }else if($stuff->type == 'L'){
           $fighter->skill_health += $stuff->bonus;
       }
       $ToolsTable->upstuff($fighter, $stuff);
        $eventTable->newEventStuff($fighter,$stuff);
       //$eventTable=newEventStuff($fighter,$stuff);
       //On save les changements
       $this->save($fighter);  
   }
   
   public function pickUP($fighter, $dir){
       list($col,$lig)=$this->ligcol();
       $ToolsTable = TableRegistry::get('tools');
        $actionPossible=$this->setActionPoints($fighter);
       if($actionPossible==='Action'){
       if(($dir=='right')&&($fighter->coordinate_x<$col)){
           $target_x = $fighter->coordinate_x + 1;
           $target_y = $fighter->coordinate_y;
           $stuff = $ToolsTable->getStuffByPos($target_x, $target_y);
           //S'il y a un equipement a la position choisie
           if(count($stuff) != 0){
               $this->upstuff($fighter, $stuff);
           }
           else{
               return('nothing');
           }
       }
        else if (($dir == 'left') && ($fighter->coordinate_x > 1)) {
            $target_x = $fighter->coordinate_x - 1;
            $target_y = $fighter->coordinate_y;
            $stuff = $ToolsTable->getStuffByPos($target_x, $target_y);
           //S'il y a un equipement a la position choisie
            if (count($stuff) != 0) {
                $this->upstuff($fighter, $stuff);
            } else {
                return('nothing');
            }
        } else if(($dir=='up')&&($fighter->coordinate_y>1)){
            $target_x = $fighter->coordinate_x;
            $target_y = $fighter->coordinate_y - 1;
            $stuff = $ToolsTable->getStuffByPos($target_x, $target_y);
           //S'il y a un equipement a la position choisie
            if (count($stuff) != 0) {
                $this->upstuff($fighter, $stuff);
            } else {
                return('nothing');
            }
       
       }
       else if(($dir=='down')&&($fighter->coordinate_y<$lig)){
            $target_x = $fighter->coordinate_x;
            $target_y = $fighter->coordinate_y + 1;
            $stuff = $ToolsTable->getStuffByPos($target_x, $target_y);
           //S'il y a un equipement a la position choisie
            if (count($stuff) != 0) {
                $this->upstuff($fighter, $stuff);
            } else {
                //return('nothing');
            }
       }
   }
   }

}