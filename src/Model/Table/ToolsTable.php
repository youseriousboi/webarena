<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class ToolsTable extends Table{
    
    public function createNewStuff(){
         $toolsTable = TableRegistry::get('tools');
         $FightersTable = TableRegistry::get('Fighters');
         //$FightersTable = $this->loadModel('Fighters');
         $stuff = $toolsTable->newEntity();
         $random = rand(0, 2);
         if ($random == 0){
             $stuff->type = 'V';
         } else if ($random == 1){
             $stuff->type = 'D';
         }else if ($random == 2){
             $stuff->type = 'L';
         }
         $stuff->bonus = rand(1, 3);
         $stuff->fighter_id = null;
         do{
            $posx = $FightersTable->getRandomPosx();
            $posy = $FightersTable->getRandomPosy();
         }while(count($FightersTable->getTargetByXY($posx, $posy)) != 0 && count($this->getStuffByPos($posx, $posy)) != 0);
         $stuff->coordinate_x = $posx;
         $stuff->coordinate_y = $posy;
         $toolsTable->save($stuff); 
    }
    
    public function getStuffByPos($posx, $posy){
        return ($this->find()->where(['coordinate_x' => $posx, 'coordinate_y' => $posy])->first());
    }
    
    public function getStuffByID($id){
        return ($this->find()->where(['fighter_id' => $id])->count());
    }
    
    public function getStuffIn(){
        //retourne les objets actifs (dont la pos est non nulle)
        return ($this->find()->where(['coordinate_x >=' => 1,'coordinate_x <=' => 15, 'coordinate_x >=' => 1,'coordinate_y <=' => 10])->count());
    }
    public function getAllTools(){
        return ($this->find()->all());
    
    }
    
    public function upstuff($fighter, $stuff){
        //On attribue l'item au player
       $stuff->fighter_id = $fighter->id;
       //On retire l'item du terrain
       $stuff->coordinate_x = null;
       $stuff->coordinate_y = null;
       $this->save($stuff);
    }
    
    public function magicSystem(){
        $diff = $this->getStuffIn();
       // $diff = 10 - count($this->getStuffIn());
        //tant que l'on a moins de 10 objets, on en recrée jusqu'a 10
        if($diff>10){
            return('full');
        }
        else{
        while($diff <10){
            //Créer des bonus dans la base
            $this->createNewStuff();
            $diff++;
        }
        return('full');
        }
    }
    
}

