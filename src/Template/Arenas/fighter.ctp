<?php $this->assign('title', 'fighter'); ?>
<html>
    <head>
        <?= $this->Html->css('fighter.css') ?>
        <?= $this->Html->css('datatable.css') ?>

        <script>
            $(document).ready(function() {
                $('#fighter_list').DataTable();
            });
            $("#add_fighter button").click(function () {
                $("#add_form").css('visibility', 'visible');
            });
        </script>
    </head>
    <body>
        <table id="fighter_list" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Avatar</th>
                    <th>Level</th>
                    <th>XP</th>
                    <th>Current_Health</th>
                    <th>Position_X</th>
                    <th>Position_Y</th>
                    <th>Skill_health</th>
                    <th>Skill_strength</th>
                    <th>Skill_sight</th>
                    <th>Niveau(x)_dispo</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Avatar</th>
                    <th>Level</th>
                    <th>XP</th>
                    <th>Current_Health</th>
                    <th>Position_X</th>
                    <th>Position_Y</th>
                    <th>Skill_health</th>
                    <th>Skill_strength</th>
                    <th>Skill_sight</th>
                    <th>Niveau(x)_dispo</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $session = $this->request->session();
                //foreach .... as fighters
                if($session->check('fightersTable')){
                    $fighterstable = $session->read('fightersTable');
                    foreach ($fighterstable as $fighter){
                        $remlvl = floor(($fighter->xp)/4 - ($fighter->level));
                        $img = $this->Html->image("default", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '20', 'width' => '20']);
                        $avatar = $this->Html->image("avatars/$fighter->id.png", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '200', 'width' => '200']);
                        $chooseavatar = $this->Form->postButton('Modification avatar', ['controller' => 'Arenas', 'action' => 'avatar', $fighter->id]);
                        //$health = $this->Form->button('Health+3', ['name' => 'health']);
                        //$attack = $this->Form->button('Attack+1', ['name' => 'attack']);
                        //$sight = $this->Form->button('Sight+1', ['name' => 'sight']);
                        $health = $this->Form->postButton('health+3', ['controller' => 'Arenas', 'action' => 'levelUp', 'health', $fighter->id, $remlvl]);
                        $attack = $this->Form->postButton('attack+1', ['controller' => 'Arenas', 'action' => 'levelUp', 'attack', $fighter->id, $remlvl]);
                        $sight = $this->Form->postButton('sight+1', ['controller' => 'Arenas', 'action' => 'levelUp', 'sight', $fighter->id, $remlvl]);
                        $default = $this->Form->postButton('Choose this fighter', ['controller' => 'Arenas', 'action' => 'defaultFighter', $fighter->id]);
                        //$debut = $this->Form->create();
                        //$fin = $this->Form->end();
                        if($remlvl < 0) $remlvl = 0;
                        echo "<tr>"
                        . "<td>$fighter->name";
                        if($session->check('myFighterID')){
                            if($session->read('myFighterID') == $fighter->id)
                                echo $img;
                        }
                        echo "$default</td>"
                        . "<td> $avatar $chooseavatar</td>"
                        . "<td>$fighter->level</td>"
                        . "<td>$fighter->xp</td>"
                        . "<td>$fighter->current_health</td>"
                        . "<td>$fighter->coordinate_x</td>"
                        . "<td>$fighter->coordinate_y</td>"
                        . "<td>$fighter->skill_health</td>"
                        . "<td>$fighter->skill_strength</td>"
                        . "<td>$fighter->skill_sight</td>"
                        . "<td>$remlvl $health $attack $sight</td>"
                        . "</tr>";

                    }
                }
                ?>
            </tbody>
        </table>


        <div id="add_fighter">
            <button type="button">+</button>
                <div id="add_form">
                    <?=
                    $this->Form->create();
                    echo $this->Form->control('pseudo');
                    echo $this->Form->button("Create");
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        <script>
            $("#add_fighter button").click(function () {
                $("#add_form").css('visibility', 'visible');
            });
        </script>
    </body>
</html>
