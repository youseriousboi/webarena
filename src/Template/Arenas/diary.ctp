<?php $this->assign('title', 'diary'); ?>
<html>
    <head>
        <?= $this->Html->css('diary.css') ?>
        <?= $this->Html->css('datatable.css') ?>

        <script>
            $(document).ready(function () {
                $('#event_list').DataTable();
            });
        </script>

    </head>
    <body>
        <table id="event_list" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Event</th> 
                </tr>
            </thead>
            <tbody>
                <?php
                $session = $this->request->session();
                if($session->check('myFighterID')){
                    $currentY = $fighter->coordinate_y;
                    $currentX = $fighter->coordinate_x;
                    $sight = $fighter->skill_sight;
                    $distX;
                    $distY;

                    for ($i = 1; $i <= $lig; $i++) {
                        for ($j = 1; $j <= $col; $j++) {
                            if ($currentX <= $j) {
                                $distX = $j - $currentX;
                            } else {
                                $distX = $currentX - $j;
                            }
                            if ($currentY <= $i) {
                                $distY = $i - $currentY;
                            } else {
                                $distY = $currentY - $i;
                            }
                            foreach ($eventstable as $event) {
                                //check si c'est dans sa vision
                                if (($event->coordinate_y == $i) && ($event->coordinate_x == $j) && ($distX + $distY <= $sight)) {
                                    echo "<tr>"
                                    . "<td> $event->date </td>"
                                    . "<td> $event->name </td>"
                                    . "</tr>\n";
                                }
                            }
                        }
                    }
                }
                ?>
            </tbody>
        </table>
    </body>
</html>