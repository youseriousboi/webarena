<?php $this->assign('title', 'index');?>



<html>
    <head>
        <link rel="stylesheet" type="text/css" href="webroot/engine1/style.css" />
        <script type="text/javascript" src="webroot/engine1/jquery.js"></script>
    </head>
    
    <body>
 
        <!-- Présentation du jeu WebArena -->
        
        <center class="text">
            
            <h1 class="text"> Bienvenue dans WebArena !</h1>
            </br>
            <p> Ce jeu a été développé dans le cadre d'un projet de développement web par les étudiants de l'école ECE Paris. </p>
            <p> Différents combattants sont présents dans une arène et doivent se combattre les uns les autres !</p>
            <p> Vous pouvez donc créer vos propres personnages, les personnaliser, et les faire combattre afin de les faire progresser et gagner en expérience.</p>
        </center>    
    
    </br>
    </br>
    
        <!-- Slider des avatars de personnages --> 
 
        <center><h2 class="text"> Les combattants présents dans l'arène </h2></center>
        
           
    </br>
        
        <div id="wowslider-container1">
        <div class="ws_images"><ul>
                <?php 
                    foreach ($allfighters as $fighter) {
                        $repertory='img\Avatars' . DS . $fighter->id . '.png';
                        echo "<li>"
                        . "<img src=\"$repertory\" alt=\"Picture\" title=\"$fighter->name\"/></a>"
                        . "</li>";
                    }
                                ?>
                </ul></div>
        <div class="ws_script" style="position:absolute;left:-99%">css image slider</a></div>
        <div class="ws_shadow"></div>
        </div>	
        <script type="text/javascript" src="engine1/wowslider.js"></script>
        <script type="text/javascript" src="engine1/script.js"></script>
        
    </br>
    </br>
    <center>
    <?php
        echo $this->Form->postButton('Se connecter', ['controller' => 'Arenas', 'action' => 'login']);
        ?>
    </center>
    
    
    
    
    </body>
    <script>
        $(document).ready(function(){
            $(".text").css('color', 'white');
        });
    </script>
</html>


<!--
//choose a character ?
/*
    $session = $this->request->session();
    if($session->check('player_email')){
        
    }
 * 
 */
?>-->