<?php $this->assign('title', 'sight'); ?>
<!DOCTYPE html>
<html>
    <head>

    </head>
    <body>
        <?php
            $session = $this->request->session();
            if($session->check('myFighterID')){
                echo "<table id='grid' class='display' width='100%' border='1'>"; 
                $currentY = $myfighter->coordinate_y;
                $currentX = $myfighter->coordinate_x;
                $sight = $myfighter->skill_sight;
                $distX;
                $distY;
                $tmp = 1;
                $tmp2=500;
                
                for ($i = 1; $i <= $lig; $i++)
                {
                    echo "<tr id=" . $i . ">";
                    echo "</tr>";

                    for ($j = 1; $j <= $col; $j++)
                    {
                        if ($currentX <= $j)
                        {
                            $distX = $j - $currentX;
                        }
                        else
                        {
                            $distX = $currentX - $j;
                        }
                        if ($currentY <= $i)
                        {
                            $distY = $i - $currentY;
                        }
                        else
                        {
                            $distY = $currentY - $i;
                        }
                        if($distX + $distY <= $sight)
                        {
                        echo "<td class=" . $tmp2 . ">";
                        $tmp++;
                        $tmp2++;    
                        }
                        else
                        {
                            echo "<td class=" . $tmp . ">";
                            $tmp ++;
                            $tmp2++;
                        }
                        foreach($alltools as $tools)
                        {
                            if(($tools->coordinate_y==$i)&&($tools->coordinate_x==$j)&&($distX + $distY <= $sight))
                            {
                                //echo "Bonus";
                                switch ($tools->type) {
                                    case 'V':
                                        if($tools->bonus == 1){
                                            echo $this->Html->image('bonus/lunette.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Sight +1', 'title'=>'Lunette - Sight +1'));
                                        }else if($tools->bonus == 2){
                                            echo $this->Html->image('bonus/jumelle.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Sight +2', 'title'=>'Jumelle - Sight +2'));
                                        }else if($tools->bonus == 3){
                                            echo $this->Html->image('bonus/satellite.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Sight +3', 'title'=>'Satellite - Sight +3'));
                                        }
                                        break;
                                    case 'D':
                                        if($tools->bonus == 1){
                                            echo $this->Html->image('bonus/decapsuleur.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Damage +1', 'title'=>'Decapsuleur - Damage +1'));
                                        }else if($tools->bonus == 2){
                                            echo $this->Html->image('bonus/shuriken.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Damage +2', 'title'=>'Shuriken - Damage +2'));
                                        }else if($tools->bonus == 3){
                                            echo $this->Html->image('bonus/kim.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Damage +3', 'title'=>'Kim - Damage +3'));
                                        }
                                        break;
                                    case 'L':
                                        if($tools->bonus == 1){
                                            echo $this->Html->image('bonus/slip.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Life +1', 'title'=>'Slip - Life +1'));
                                        }else if($tools->bonus == 2){
                                            echo $this->Html->image('bonus/casque.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Life +2', 'title'=>'Casque - Life +2'));
                                        }else if($tools->bonus == 3){
                                            echo $this->Html->image('bonus/veste.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Life +3', 'title'=>'Gilet Pare Balles - Life +3'));
                                        }
                                        break;
                                }
                            }
                        }
                        foreach ($allfighters as $fighter) {

                            if (($fighter->coordinate_y == $i) && ($fighter->coordinate_x == $j)) {
                               if(($fighter->coordinate_y==$myfighter->coordinate_y)&&($fighter->coordinate_x==$myfighter->coordinate_x)) // if the fighter is my fighter
                                    {
                                       //Affiche le combattant par defaut
                                       //echo $this->Html->image('Avatars/cartman.png', array('width' => '60px', 'height' => '60px', 'alt'=>'Default Fighter', 'title'=>'Default Fighter', 'id'=>'default'));
                                       echo "<span id=default>$myfighter->id</span>";
                                    }
                                    else if($distX + $distY <= $sight){
                                        //Affiche les autres combattants du player
                                        echo "<span class=fighters>$fighter->id</span>";
                                    }
                            }
                        }
                        if ($distX + $distY <= $sight)
                        {
                            echo ".</td>"; //display
                        }
                        else
                        {
                            echo "</td>";
                        }
                    }
                }
                echo "</table>";
                echo "<span id=showme>You've got ". $actpoints. " action point(s) ¯\_ツ_/¯ !</span>";
            }
        ?>
        <table id="controls" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th><h3>Move</h3></th>
                    <th></th>
                    <th></th>
                    <th><h3>Pick this Bonus</h3></th>
                    <th></th>
                    <th></th>
                    <th><h3>Attack BOI</h3></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $session = $this->request->session();
                if($session->check('myFighterID')){
                    $right_m = $this->Form->postButton('Right', ['controller' => 'Arenas', 'action' => 'deplacement', $myfighter->id, 'droite']);
                    $left_m = $this->Form->postButton('Left', ['controller' => 'Arenas', 'action' => 'deplacement', $myfighter->id, 'gauche']);
                    $up_m = $this->Form->postButton('Up', ['controller' => 'Arenas', 'action' => 'deplacement', $myfighter->id, 'haut']);
                    $down_m = $this->Form->postButton('Down', ['controller' => 'Arenas', 'action' => 'deplacement', $myfighter->id, 'bas']);
                    $right_a = $this->Form->postButton('Right', ['controller' => 'Arenas', 'action' => 'attack', $myfighter->id, 'right']);
                    $left_a = $this->Form->postButton('Left', ['controller' => 'Arenas', 'action' => 'attack', $myfighter->id, 'left']);
                    $up_a = $this->Form->postButton('Up', ['controller' => 'Arenas', 'action' => 'attack', $myfighter->id, 'up']);
                    $down_a = $this->Form->postButton('Down', ['controller' => 'Arenas', 'action' => 'attack', $myfighter->id, 'down']);
                    $pick_r = $this->Form->postButton('Right', ['controller' => 'Arenas', 'action' => 'pickup', $myfighter->id, 'right']);
                    $pick_l = $this->Form->postButton('Left', ['controller' => 'Arenas', 'action' => 'pickup', $myfighter->id, 'left']);
                    $pick_u = $this->Form->postButton('Up', ['controller' => 'Arenas', 'action' => 'pickup', $myfighter->id, 'up']);
                    $pick_d = $this->Form->postButton('Down', ['controller' => 'Arenas', 'action' => 'pickup', $myfighter->id, 'down']);
                    $generator = $this->Form->postButton('Magic', ['controller' => 'Arenas', 'action' => 'generation']);
                    echo "<tr>"
                    . "<td></td>"
                    . "<td>$up_m</td>"
                    . "<td></td>"
                    . "<td></td>"
                    . "<td>$pick_u</td>"
                    . "<td></td>"
                    . "<td></td>"
                    . "<td>$up_a</td>"
                    . "<td></td>"
                    . "</tr>"
                    . "<tr>"
                    . "<td>$left_m</td>"
                    . "<td></td>"
                    . "<td>$right_m</td>"
                    . "<td>$pick_l</td>"
                    . "<td>$generator</td>"
                    . "<td>$pick_r</td>"
                    . "<td>$left_a</td>"
                    . "<td></td>"
                    . "<td>$right_a</td>"
                    . "</tr>"
                    . "<tr>"
                    . "<td></td>"
                    . "<td>$down_m</td>"
                    . "<td></td>"
                    . "<td></td>"
                    . "<td>$pick_d</td>"
                    . "<td></td>"
                    . "<td></td>"
                    . "<td>$down_a</td>"
                    . "<td></td>"
                    . "</tr>";
                }
                ?>
            </tbody>
        </table>

        <script>
            $( document ).ready(function(){
                /*
                $('#controls').DataTable(
                    //"paging":   false,
                    //"ordering": false,
                    //"info":     false;
                );
                //passage de la grille en datatable 
                
                $('#grid').DataTable(
                    "paging":   false,
                    "ordering": false,
                    "info":     false;
                    //création du damier
                    "createdCell": function (td, cellData, rowData, row, col) {    
                        
                        if(row%2 === 0 && col%2 === 0){
                             //$(td).css('color', 'green')
                            //$('td'+i).css('background-image', 'url(../webroot/img/damier/grassinvisible.png)');
                            $(td).css('background-image', 'url(../webroot/img/damier/grassinvisible.png)');
                        }else{
                            $(td).css('background-image', 'url(../webroot/img/damier/grassvisible.png)');
                        }
                                                

                    };
                    
                    //affichage des avatars
                    table.rows().every( function ( rowIdx, colIndex, rowLoop ) {       
                        var cell = table.cell({ row: rowIdx, column: colIndex }).node();
                        $(cell).css('background-image', 'url(../webroot/img/damier/grassinvisible.png)');        
                    });
                   
                );
                */
                //var tmp = 0;
                for(i=1; i<=150; i++){
                    if(i%2 === 0){
                        $('.'+i).css('background-image', 'url(../webroot/img/damier/grassinvisiblefog.png)');
                    }else{
                        $('.'+i).css('background-image', 'url(../webroot/img/damier/grassvisiblefog.png)');
                    }
                }
                
                                for(j=500; j<650; j++)
                {
                       if(j%2 === 1 ){
                        $('.'+j).css('background-image', 'url(../webroot/img/damier/grassinvisible.png)');
                    }else{
                        $('.'+j).css('background-image', 'url(../webroot/img/damier/grassvisible.png)');
                    }
                }
        
                //var img = $("#default img");
                var namedefault = $("#default").text();
                $( "#default" ).replaceWith("<img src='../webroot/img/ImagePersos/"+namedefault+".png' title='You are this...thing'>");
                $('.fighters').each(function() {
                    var namefighter = $(this).text();
                    //alert(namefighter);
                    $(this).replaceWith("<img src='../webroot/img/ImagePersos/"+namefighter+".png' title="+$(this).text()+">");
                });
                
                $("#showme").css('color', 'red');
                $("#showme").css('font-weight', 'bold');
                $("#showme").css('font-size', '200%');
                $("#showme").css('padding-left', '33%');
                $("#grid td").css('height', '142px');
                $("#grid td").css('width', '126px');
            }); 
        </script>
    </body>
</html>
