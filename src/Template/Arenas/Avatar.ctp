<?php $this->assign('title', 'Avatar');?>

<html>
    <head>
        

    </head>
    
    <body> 
    
    <?php 

    echo $this->Form->postButton($this->Html->image("/webroot/img/avatars/butters.png", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '50', 'width' => '50']), ['controller' => 'Arenas', 'action' => 'AvatarModif', $IDfighter, 'butters']);;
    echo $this->Form->postButton($this->Html->image("/webroot/img/avatars/cartman.png", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '50', 'width' => '50']), ['controller' => 'Arenas', 'action' => 'AvatarModif', $IDfighter, 'cartman']);;
    echo $this->Form->postButton($this->Html->image("/webroot/img/avatars/kenny.png", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '50', 'width' => '50']), ['controller' => 'Arenas', 'action' => 'AvatarModif', $IDfighter, 'kenny']);;
    echo $this->Form->postButton($this->Html->image("/webroot/img/avatars/kyle.png", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '50', 'width' => '50']), ['controller' => 'Arenas', 'action' => 'AvatarModif', $IDfighter, 'kyle']);;
    echo $this->Form->postButton($this->Html->image("/webroot/img/avatars/stan.png", ['class' => 'default', 'alt' => 'default_fighter', 'height' => '50', 'width' => '50']), ['controller' => 'Arenas', 'action' => 'AvatarModif', $IDfighter, 'stan']);
    ?> 
    <?=$this->Form->create('UploadAvatar', array('type'=>'file'));?>
    <?= $this->Form->input('Avatar_file',array('label'=>'Votre avatar', 'type' => 'file'));?>

    <?= $this->Form->submit();?>
    <?= $this->Form->end();?>

    </body>
    
</html>