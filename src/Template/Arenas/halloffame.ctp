<?php $this->assign('title', 'hall_of_fame'); ?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../webroot/js/jqplot/jquery.jqplot.css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/jquery.jqplot.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/plugins/jqplot.bubbleRenderer.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/plugins/jqplot.dateAxisRenderer.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/plugins/jqplot.barRenderer.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/plugins/jqplot.pieRenderer.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
        <script type="text/javascript" src="../webroot/js/jqplot/plugins/jqplot.pointLabels.js"></script>
        <script>$("body").css('background-image', 'none');</script>
    </head>

    <body>
        <div class="stat">The average level of the fighters on this server is: <?php echo $avglvl?> (づ◔ ͜ʖ◔)づ </div>
        <div class="stat">There are <?php echo $nbfighters?> fighters on the server spread the game BOI ʢ ﾟ ͜ʖ ﾟʡ </div>
        <div id="chart1"></div>
        <div id="chart2"></div>
        <div id="chart3"></div>
        <div id="chart4"></div>
        
        <div id="hide">
            <!-- You saw nothing BOI :) -->
            <div id="bf">
                <span id="bflvl"><?php echo $bf->xp?></span>
                <span id="namefighter"><?php echo $bf->name?></span>
                <span id="bfhealth"><?php echo $bf->skill_health?></span>
                <span id="bfstrength"><?php echo $bf->skill_strength?></span>
                <span id="bfsight"><?php echo $bf->skill_sight?></span>
            </div>
            <div id="connection">
                <?php
                    $tmp = 0; $tmp1 = 0;
                    foreach($tablenb as $nb){
                        echo "<span id=nb".$tmp.">$nb</span><br>";
                        $tmp++; 
                    }
                    foreach($tableday as $day){
                        echo "<span id=day".$tmp1.">$day</span><br>";
                        $tmp1++; 
                    }
                ?>
            </div>
            <div id="bonusbf">
                <?php
                    $tmp2 = 0; $tmp3 = 0;
                    foreach($bestfighters as $fighter){
                        echo "<span id=name".$tmp2.">$fighter->name</span><br>";
                        echo "<span id=xp".$tmp2.">$fighter->xp</span><br>";
                        $tmp2++; 
                    }
                    foreach($stackbonus as $nbbonus){
                        echo "<span id=bonus".$tmp3.">$nbbonus</span><br>";
                        $tmp3++; 
                    }
                ?>
            </div>
            <div id="reroll">
                <?php
                    $tmp4 = 0; $tmp5 = 0; $tmp6 = 0;
                    foreach($bestfightersR as $fighterR){
                        echo "<span id=player".$tmp4.">$fighterR</span><br>";
                        $tmp4++; 
                    }
                    foreach($stacklvl as $level){
                        echo "<span id=lvl".$tmp6.">$level</span><br>";
                        $tmp6++;
                    }
                    foreach($stackreroll as $nbreroll){
                        echo "<span id=reroll".$tmp5.">$nbreroll</span><br>";
                        $tmp5++; 
                    }
                ?>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $("#hide").css('display', 'none');
                
                graph1();
                graph2();
                graph3();
                graph4();
                
                $("#chart1").css('margin-top', '2%');
                $("#chart2").css('margin-top', '5%');
                $("#chart3").css('margin-top', '5%');
                $("#chart4").css('margin-top', '5%');
                $("#chart4").css('margin-bottom', '5%');
                $(".stat").css('font-weight', 'bold');
                $(".stat").css('font-size', '150%');
                $(".stat").css('margin-top', '2%');
            });
            
            function graph1(){
                //stat du perso le plus fort graphique bar
                $.jqplot.config.enablePlugins = true;
                var bflvl = $( "#bflvl" ).text();
                var namef = $( "#namefighter" ).text();
                var bfhealth = $( "#bfhealth" ).text();
                var bfstrength = $( "#bfstrength" ).text();
                var bfsight = $( "#bfsight" ).text();
                var s1 = [bflvl, bfhealth, bfstrength, bfsight];
                var ticks = ['XP', 'Skill_Health', 'Skill_Strength', 'Skill_Sight'];

                plot1 = $.jqplot('chart1', [s1], {
                    // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                    animate: !$.jqplot.use_excanvas,
                    title: 'Best fighter ('+namef+') characteristics',
                    seriesDefaults:{
                        renderer:$.jqplot.BarRenderer,
                        pointLabels: { show: true }
                    },
                    axes: {
                        xaxis: {
                            renderer: $.jqplot.CategoryAxisRenderer,
                            ticks: ticks
                        }
                    },
                    highlighter: { show: false }
                });

                $('#chart1').bind('jqplotDataClick', 
                    function (ev, seriesIndex, pointIndex, data) {
                        $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
                    }
                );
            }
            
            function graph2(){
                //nb d'event sur les 7 derniers jours
                var line1=[[$("#day0").text(), $("#nb0").text()], [$("#day1").text(), $("#nb1").text()], [$("#day2").text(), $("#nb2").text()], [$("#day3").text(), $("#nb3").text()], [$("#day4").text(), $("#nb4").text()], [$("#day5").text(), $("#nb5").text()], [$("#day6").text(), $("#nb6").text()]];
                var plot1 = $.jqplot('chart2', [line1], {
                    title:'Activity on the game (number of events)',
                    axes:{
                        xaxis:{
                            renderer:$.jqplot.DateAxisRenderer
                        }
                    },
                    series:[{lineWidth:4, markerOptions:{style:'square'}}]
                });
            }
            
            function graph3(){
                //nb de bonus pour les 5 meilleurs fighters
                $bonus0 = parseInt($("#bonus0").text());
                $xp0 = parseInt($("#xp0").text());
                $bonus1 = parseInt($("#bonus1").text());
                $xp1 = parseInt($("#xp1").text());
                $bonus2 = parseInt($("#bonus2").text());
                $xp2 = parseInt($("#xp2").text());
                $bonus3 = parseInt($("#bonus3").text());
                $xp3 = parseInt($("#xp3").text());
                $bonus4 = parseInt($("#bonus4").text());
                $xp4 = parseInt($("#xp4").text());
                var arr = [[$bonus0, $xp0, 1500, $("#name0").text()], [$bonus1, $xp1, 1400, $("#name1").text()], 
                [$bonus2, $xp2, 1300, $("#name2").text()], [$bonus3, $xp3, 1200, $("#name3").text()], 
                [$bonus4, $xp4, 1100, $("#name4").text()]];
                var plot1 = $.jqplot('chart3',[arr],{
                    title: 'Number of bonus of the 5 best fighters order by their xp',
                    seriesDefaults:{
                        renderer: $.jqplot.BubbleRenderer,
                        rendererOptions: {
                            bubbleGradients: true
                        },
                        shadow: true
                    }
                });
            }
            
            function graph4(){
                //nb de reroll pour les 5 meilleurs fighters
                $reroll0 = parseInt($("#reroll0").text());
                $lvl0 = parseInt($("#lvl0").text());
                $reroll1 = parseInt($("#reroll1").text());
                $lvl1 = parseInt($("#lvl1").text());
                $reroll2 = parseInt($("#reroll2").text());
                $lvl2 = parseInt($("#lvl2").text());
                $reroll3 = parseInt($("#reroll3").text());
                $lvl3 = parseInt($("#lvl3").text());
                $reroll4 = parseInt($("#reroll4").text());
                $lvl4 = parseInt($("#lvl4").text());
                var arr = [[$reroll0, $lvl0, 1500, $("#player0").text()], [$reroll1, $lvl1, 1400, $("#player1").text()], 
                [$reroll2, $lvl2, 1300, $("#player2").text()], [$reroll3, $lvl3, 1200, $("#player3").text()], 
                [$reroll4, $lvl4, 1100, $("#player4").text()]];
                var plot1 = $.jqplot('chart4',[arr],{
                    title: 'Number of reroll of the 5 strongest players order by their best fighter xp',
                    seriesDefaults:{
                        renderer: $.jqplot.BubbleRenderer,
                        rendererOptions: {
                            bubbleGradients: true
                        },
                        shadow: true
                    }
                });
            }
        </script>
    </body>

</html>