<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
* Personal Controller
* User personal interface
*
*/
class ArenasController  extends AppController
{
  public function index(){
      $this->loadModel('Fighters');
      $this->set('allfighters', $this->Fighters->getAll());
      $playerName = "";
      $session = $this->request->session();
      if($session->check('email')){
          $playerName = $session->read('email');
      }
      $this->set('myname', $playerName);
  }

    public function login(){

        //form processing
        if($this->request->is("post")){ //$this->request all informations about the request
                    //processing
            $login = $this->request->getData("email");   //equivalent to $_POST["login"];
            $password = $this->request->getData("password");

            for($i = 0; $i < 100; $i++){    //hash 100 times for better security
                $password = md5($password);
            }

            $playersTable = $this->loadModel("players");
            $query = $playersTable->find('all')->where(['email' => $login])->andWhere(['password' => $password]);
            if($query->count() != 0){
                $id = $playersTable->find('list')->where(['email' => $login])->first();
                $session = $this->request->session();
                $session->write('email', $login);
                $session->write('playerID', $id);
                return $this->redirect(array('action' => 'fighter'));
            }
            else{
                $this->Flash->success(__('mot de passe ou nom d utilisateur incorrect'));
            }
        }
    }

   public function avatar($fighterID)
    {
            
        /// Loading fighter and getting the fighter thanks to his ID
            
        $this->loadModel('Fighters');
        $fighter= $this->Fighters->getMyFighterByFighterId($fighterID);
        
        $this->set('IDfighter', $fighterID);
        
        /// If the user has published something thanks to the form
        
        if(!empty($this->request->data))
        {
            
            /// Obtain the extension of the picture file
            
            $extension=strtolower(pathinfo($this->request->data['Avatar_file']['name'], PATHINFO_EXTENSION));
            
            /// If the form is not empty and the file sent is a picture
            
            if(!empty($this->request->data['Avatar_file']['tmp_name']) && in_array($extension, array('jpg', 'jpeg', 'png')))
            {
                
                /// Send the uploaded file to "webroot/img/Avatars" with the name "Character's name.png"
                
                move_uploaded_file($this->request->data['Avatar_file']['tmp_name'], WWW_ROOT.'\img\imagepersos/' . DS . $fighter->id . '.png');
                
                /// Copy the file sent to "webroot/img/Avatars" to "webroot/img/imagePersos"
                
               // copy(WWW_ROOT.'\img\Avatars/' . DS . $fighter->name . '.png', WWW_ROOT.'\img\imagePersos/' . DS . $fighter->name . '.png');
            
                
                /// The size of the uploaded picture is stocked into the variable "$img_size"
                $img_size = getimagesize(WWW_ROOT.'img\imagepersos/' . DS . $fighter->id . '.png');
                
                /// And the "height" and "weight" attributes are stocked 
                $W_Src = $img_size[0];
                $H_Src = $img_size[1];

                /// Obtaining the dimensions of the avatar's background
                $imgDest_size = getimagesize(WWW_ROOT.'img\FondAvatar.png');

                /// "Height" and "weight" attributes are stocked 
                $W_Max = $imgDest_size[0];
                $H_Max = $imgDest_size[1];
                
                /// Creation of a "picture" for the last Avatar, which is going to be displayed
                $LastPic = imagecreatefrompng(WWW_ROOT.'img\FondAvatar.png');
 
                /// Creation of a "picture" item of the appropriate size for the intermediate picture
                if($W_Src < $H_Src)
                {
                    $img_dest= imagecreatetruecolor($W_Src*($H_Max/$H_Src), $H_Max);
                }
                else
                {
                    $img_dest= imagecreatetruecolor($W_Max, $H_Src*($W_Max/$W_Src));
                }
                
                /// Creation of a "picture" item from the picture in "webroot/img/Avatars"
                if(in_array($extension, array('jpg', 'jpeg')))
                {
                    $Source_img = imagecreatefromjpeg(WWW_ROOT.'img\imagepersos/' . DS . $fighter->id . '.png');
                }
                else 
                {
                    $Source_img = imagecreatefrompng(WWW_ROOT.'img\imagepersos/' . DS . $fighter->id . '.png');
                }
                
                if($H_Src>$W_Src)
                {
                    if($H_Src > $H_Max)
                    {
                        imagecopyresampled($img_dest, $Source_img, 0, 0, 0, 0, $W_Src*($H_Max/$H_Src), $H_Max, $W_Src, $H_Src);
                        imagecopyresampled($LastPic, $img_dest, ($H_Max/2)-(($W_Src*($H_Max/$H_Src))/2), 0, 0, 0, $W_Src*($H_Max/$H_Src), $H_Max, $W_Src*($H_Max/$H_Src), $H_Max);
                        imagepng($LastPic, WWW_ROOT.'img\Avatars' . DS . $fighter->id . '.png');
                    }
                }
                else
                {
                    if($W_Src > $W_Max)
                    {
                        imagecopyresampled($img_dest, $Source_img, 0, 0, 0, 0, $W_Max, $H_Src*($W_Max/$W_Src), $W_Src, $H_Src);
                        imagecopyresampled($LastPic, $img_dest, 0, ($W_Max/2)-(($H_Src*($W_Max/$W_Src))/2), 0, 0, $W_Max,  $H_Src*($W_Max/$W_Src), $W_Max, $H_Src*($W_Max/$W_Src));
                        imagepng($LastPic, WWW_ROOT.'img\Avatars' . DS . $fighter->id . '.png');
                    }
                }
            }
            
                    return $this->redirect(['action' => 'fighter']);
                
        }

    }

 
    public function avatarModif($fighterID, $avatarName)
    {
        $this->loadModel('Fighters');
        $fighter=$this->Fighters->getMyFighterByFighterId($fighterID);
        copy("..\webroot\img\Avatars/$avatarName.png", "..\webroot\img\Avatars/$fighter->id.png");
        copy("..\webroot\img\Avatars/$fighter->id.png", "..\webroot\img\ImagePersos/$fighter->id.png");
        
        
        /// The size of the uploaded picture is stocked into the variable "$img_size"
                $img_size = getimagesize(WWW_ROOT.'img\Avatars/' . DS . $fighter->id . '.png');
                
                /// And the "height" and "weight" attributes are stocked 
                $W_Src = $img_size[0];
                $H_Src = $img_size[1];

                /// Obtaining the dimensions of the avatar's background
                $imgDest_size = getimagesize(WWW_ROOT.'img\FondAvatar.png');

                /// "Height" and "weight" attributes are stocked 
                $W_Max = $imgDest_size[0];
                $H_Max = $imgDest_size[1];
                
                /// Creation of a "picture" for the last Avatar, which is going to be displayed
                $LastPic = imagecreatefrompng(WWW_ROOT.'img\FondAvatar.png');
                
                
                /// Creation of a "picture" item of the appropriate size for the intermediate picture
                if($W_Src < $H_Src)
                {
                    $img_dest= imagecreatetruecolor($W_Src*($H_Max/$H_Src), $H_Max);
                }
                else
                {
                    $img_dest= imagecreatetruecolor($W_Max, $H_Src*($W_Max/$W_Src));
                }
                
                /// Creation of a "picture" item from the picture in "webroot/img/Avatars"
                $Source_img = imagecreatefrompng(WWW_ROOT.'img\imagepersos' . DS . $fighter->id . '.png');

                


                        imagecopyresampled($LastPic, $Source_img, 0, 0, 0, 0, $W_Max, $H_Src*($W_Max/$W_Src), $W_Src, $H_Src);
                        imagepng($LastPic, WWW_ROOT.'img\Avatars' . DS . $fighter->id . '.png');

        
        
        
        
        return $this->redirect(['action' => 'fighter']);
    }

    public function fighter(){
        $session = $this->request->session();
        $this->loadModel('Fighters');

        //nouveaux utilisateurs
        if($session->check('email')){
            if(! $session->check('playerID')){
                $playersTable = $this->loadModel('players');
                if($playersTable->find()->count() == 0){
                    $this->Flash->success(__('créez un nouveau combattant avec le boutton "+"'));
                }
                //write the player ID in the session
                $playerID = $this->players->find('list')->where(['email' => $session->read('email')]);
                if($playerID != null){
                    $session->write('playerID', $playerID);
                }
            }
            if(! $session->check('myFighterID')){
                $this->Flash->success(__('choisissez un combattant'));
            }
            $session->write('fightersTable', $this->Fighters->getMyFighterByPlayerID($session->read('playerID')));
            $playerName = $session->read('email');
        }else{
            $this->Flash->success(__('connectez vous pour accéder à vos combattants'));
        }

        if($this->request->is("post")){ //create new fighter
            if($session->check('playerID')){
                $fighter = $this->Fighters->createNewFighter($session->read('playerID'),$this->request->getData("pseudo"));
                copy("..\webroot\img\Avatars\NaziZombie.png", "..\webroot\img\imagepersos/$fighter->id.png");
                return $this->redirect(array('action' => 'fighter'));
            }
        }
    }

    public function sight(){
        $this->loadModel('Fighters');
        $this->loadModel('Tools');
        $session = $this->request->session();
        $myFighter = "";
        if( $session->check('myFighterID') ){
            $myFighter = $this->Fighters->getMyFighterByFighterId($session->read('myFighterID'));
            $currentTime=Time::now();
            $refDate=$myFighter->next_action_time;
            $diff=$refDate->diff($currentTime);
            $this->set('actpoints',(int)($diff->s/10));
        }else{
            $this->Flash->success(__('Connectez vous et choisissez un combattant pour accéder à vision'));
        }
        list($col,$lig)=$this->Fighters->ligcol();
        $this->set('col',$col);
        $this->set('lig',$lig);
        $this->set('allfighters', $this->Fighters->getAll());
        $this->set('alltools',$this->Tools->getAllTools());
        $this->set('myfighter',$myFighter); // L'id dependra de la session ( en du fighter selectionné par le joueur )
        $this->set('diff',$this->Tools->getStuffIn());
    }

    public function forgotPassword(){

         if($this->request->is("post")){ //$this->request all informations about the request
                    //processing
            $playersTable = $this->loadModel("players");
            $email = $this->request->getData("email");
            $query = $playersTable->find('all')->where(['email' => $email]);
            if($query->count() != 0){
                $playerssTable = TableRegistry::get('players');
                $player = $query->first();
                $password = $this->request->getData("password");
                for($i = 0; $i < 100; $i++){
                    $password = md5($password);
                }
                $player->password = $password;
                if($playersTable->save($player)){
                    $this->Flash->success(__("mot de passe mis à jour"));
                }                
            }else{
                $this->Flash->success(__('e-mail non trouvé'));
            }
        }
    }


      public function diary(){
        $this->loadModel('Events');
        $this->loadModel('Fighters');
        $myFighter = "";
        list($col,$lig)=$this->Fighters->ligcol();
        $this->set('col',$col);
        $this->set('lig',$lig);
        $session = $this->request->session();
        if($session->check('myFighterID')){
            $myFighter = $this->Fighters->getMyFighterByFighterId($session->read('myFighterID'));
        }else{
            $this->Flash->success(__("connectez-vous pour afficher les évènements"));
        }
        $this->set('fighter', $myFighter);
        $this->set('eventstable', $this->Events->getAllEvents());
    }

    public function logout(){
        $session = $this->request->session();
        $session->destroy();
        return $this->redirect(array('action' => '/'));
    }

    public function register(){
        if($this->request->is("post")){
            //check the email doesn't belong to the database
            $email = $this->request->getData("email");
            $innerTable = $this->loadModel("players");
            $query = $innerTable->find('all')->where(['email' => $email]);
            if($query->count() == 0){
                //check email is not already in the
                //add new entry in the database
                $playersTable = TableRegistry::get('players');
                $player = $playersTable->newEntity();
                $player->email = $email;
                $password = $this->request->getData("password");
                for($i = 0; $i < 100; $i++){
                    $password = md5($password);
                }
                $player->password = $password;
                if ($playersTable->save($player)) {
                    // The $player entity contains the id now
                    $session = $this->request->session();
                    $session->write('email', $email);
                    //redirect onto index
                    return $this->redirect(array('action' => '/'));
                }
            }else{
                $this->Flash->success(__('e-mail already used'));
            }
        }
    }

    public function deplacement($fighterID, $dir){
        $this->loadModel('Fighters');
        if ($this->request->is(['post'])) {
            $this->Fighters->deplacement($this->Fighters->getMyFighterByFighterId($fighterID),$dir);
            // redirect to another action
            return $this->redirect(['action' => 'sight']);
        }
    }


    public function attack($fighterID, $dir){
        $this->loadModel('Fighters');

            if ($this->request->is(['post'])) {
                $this->Fighters->attack($this->Fighters->getMyFighterByFighterId($fighterID),$dir);
                /*if($this->Fighters->attack($this->Fighters->getMyFighterByFighterId($fighterID),$dir) == 'hit'){
                    $this->Flash->success(__('Hit !!'));
                }else if ($this->Fighters->attack($this->Fighters->getMyFighterByFighterId($fighterID),$dir) == 'kill'){
                    $this->Flash->success(__('You got it BOI !!'));
                }else if ($this->Fighters->attack($this->Fighters->getMyFighterByFighterId($fighterID),$dir) == 'noone'){
                    $this->Flash->success(__('No target here BOI!!'));
                }else if ($this->Fighters->attack($this->Fighters->getMyFighterByFighterId($fighterID),$dir) == 'fail'){
                    $this->Flash->success(__('Attack failed BOI RNG at its best :o '));
                }*/
                // redirect to another action
                return $this->redirect(['action' => 'sight']);
            }
    }

    public function levelUp($stat, $id, $lvl){
        $this->loadModel('Fighters');
        if ($lvl > 0){
            if ($this->request->is(['post'])) {
                $this->Fighters->level_up($stat, $id);
                // redirect to another action
                $this->Flash->success(__('NANI !!?'));
                return $this->redirect(['action' => 'fighter']);
            }
        }
        else{
            $this->Flash->success(__('You blind you ⤜(ʘ_ʘ)⤏ you do not have any level available  (╯°□°）╯︵ ┻━┻'));
            return $this->redirect(['action' => 'fighter']);
            //echo '<script type="text/javascript">window.alert("Aucun niveau disponible!!");</script>';

        }
    }

    public function pickup($fighterID, $dir) {
        $this->loadModel('Fighters');
        $this->loadModel('Tools');
        if ($this->request->is(['post'])) {
            if($this->Fighters->pickUP($this->Fighters->getMyFighterByFighterId($fighterID), $dir) == 'nothing'){
                $this->Flash->success(__('No bonus here you fool !!'));
                return $this->redirect(['action' => 'sight']);
            }else{
                return $this->redirect(['action' => 'sight']);
            }
        }
    }

    public function generation(){
        $this->loadModel('Tools');
        if ($this->request->is(['post'])) {
            if($this->Tools->magicSystem() == 'full'){
                $this->Flash->success(__('Stop abusing magic mana is not free !!'));
                return $this->redirect(['action' => 'sight']);
            }
        }
    }

    public function defaultFighter($fighterID){
        $this->loadModel('Fighters');
        $fighter = $this->Fighters->getMyFighterByFighterId($fighterID);
        //Ecrire dans la session une ligne default_fighter = $fighter->id;
        $session = $this->request->session();
        $session->write('myFighterID', $fighter->id);
        return $this->redirect(['action' => 'fighter']);
    }

      public function halloffame(){
        $this->loadModel('Fighters');
        $this->loadModel('Events');
        //graph1
        $this->set('bf' ,$this->Fighters->getBestFighter());
        //graph2
        list($tablenb,$tableday)=$this->Events->eventActivity();
        $this->set('tablenb', $tablenb);
        $this->set('tableday', $tableday);
        //graph3
        list($bestfighters,$stackbonus)=$this->Fighters->getBestFightersBonus();
        $this->set('bestfighters', $bestfighters);
        $this->set('stackbonus', $stackbonus);
        //graph4
        $bestfightersR = $this->Fighters->getBestFighters();
        $tableplayer = array();
        $tablelvl = array();
        $tablereroll = array();
        $tmp6 = 0;
        $tmp7 = null;
        foreach ($bestfightersR as $fighterR) {
            if ($fighterR->player_id != $tmp7 && $tmp6 < 5){
                array_push($tableplayer, $fighterR->player_id);
                array_push($tablelvl, $fighterR->xp);
                array_push($tablereroll, $this->Fighters->countReroll($fighterR->player_id));
                $tmp7 = $fighterR->player_id;
                $tmp6++;
            }
        }
        $this->set('bestfightersR', $tableplayer);
        $this->set('stacklvl', $tablelvl);
        $this->set('stackreroll', $tablereroll);
        //other informations
        $this->set('avglvl', $this->Fighters->getAll()->avg('level'));
        $this->set('nbfighters', $this->Fighters->getAll()->count());
    }
}
